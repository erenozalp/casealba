<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class CardController extends Controller
{
    public function index(Request $request)
    {
        $user = $request->user();
        // $cart_items = Cache::get("CART_{$user->id}", []);
        // $cart = Collection::make($cart_items)->groupBy('id');
        $cart = $user->itemsCart();

        return view('cart.index', compact('cart'));
    }

    public function addToCard(Request $request, Product $product)
    {
        $user = $request->user();

        $user->addCart($product);
        return redirect()->route('product.index');
    }

    public function removeFromCard(Request $request, Product $product)
    {
        $user = $request->user();

        $user->addCart($product, -1);

        return redirect()->route('cart');
    }

    public function clearFromCard(Request $request, Product $product)
    {
        $user = $request->user();

        $user->removeCart($product);

        return redirect()->route('cart');
    }
}
