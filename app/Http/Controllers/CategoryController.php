<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::paginate();

        return view('category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $user = $request->user();

        if (!$user->can('management.content'))
            return abort(403, 'USER DOES NOT HAVE THE RIGHT PERMISSIONS.');

        $category = new Category();
        return view('category.form', compact('category'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $user = $request->user();

        if (!$user->can('management.content'))
            return abort(403, 'USER DOES NOT HAVE THE RIGHT PERMISSIONS.');

        $data = $request->validated();
        $category = Category::create($data);

        return redirect()->route('category.index')->with('success', __('Category Created Succesfuly.'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Category $category)
    {
        $user = $request->user();

        if (!$user->can('management.content'))
            return abort(403, 'USER DOES NOT HAVE THE RIGHT PERMISSIONS.');
        return view('category.form', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, Category $category)
    {
        $user = $request->user();

        if (!$user->can('management.content'))
            return abort(403, 'USER DOES NOT HAVE THE RIGHT PERMISSIONS.');

        $data = $request->validated();
        $category->update($data);

        return redirect()->route('category.index')->with('success', __('Category Updated Succesfuly.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        if ($category->product()->exists())
            return redirect()->route('category.index')->with('warning', __('First you need to remove product(s) of the category.'));
        $category->delete();

        return redirect()->route('category.index')->with('success', __('Category Deleted Succesfuly.'));
    }
}
