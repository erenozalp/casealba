<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $category = null;

        if ($request->get('category'))
            $category = Category::findOrFail($request->get('category'));

        $products = Product::when($category, function ($query) use ($category) {
            $query->ofCategory($category);
        })->paginate()->appends(['category' => $category?->id]);

        return view('product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = $request->user();
        if (!$user->can('management.content'))
            return abort(403, 'USER DOES NOT HAVE THE RIGHT PERMISSIONS.');

        $product = new Product();
        $categories = Category::all();
        return view('product.form', compact('product', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreProductRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        $user = $request->user();
        if (!$user->can('management.content'))
            return abort(403, 'USER DOES NOT HAVE THE RIGHT PERMISSIONS.');

        $data = $request->validated();

        $product = Product::create($data);

        if ($request->hasFile('images')) {
            $product->addMultipleMediaFromRequest(['images'])
                ->each(function ($file) {
                    $file->toMediaCollection('images');
                });
        }

        return redirect()->route('product.index')->with('success', __('Product Created Succesfuly.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Product $product)
    {
        $user = $request->user();
        if (!$user->can('management.content'))
            return abort(403, 'USER DOES NOT HAVE THE RIGHT PERMISSIONS.');

        $categories = Category::all();
        return view('product.form', compact('product', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateProductRequest  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
        $user = $request->user();
        if (!$user->can('management.content'))
            return abort(403, 'USER DOES NOT HAVE THE RIGHT PERMISSIONS.');

        $data = $request->validated();
        $product->update($data);

        if ($request->hasFile('images')) {
            $product->addMultipleMediaFromRequest(['images'])
                ->each(function ($file) {
                    $file->toMediaCollection('images');
                });
        }
        return redirect()->route('product.index')->with('success', __('Product Updated Succesfuly.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('product.index')->with('success', __('Product Deleted Succesfuly.'));
    }
}
