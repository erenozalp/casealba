<?php

namespace App\Models;

use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'phone',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeSearch(Builder $query, ?string $search = null): Builder
    {
        return $query->when($search, function ($query) use ($search) {
            $query->whereRaw("concat(first_name, ' ', last_name) like '%" . $search . "%' ");
        });
    }

    private function cartKey(): string
    {
        return "CART_{$this->id}";
    }

    public function getCart(): Collection
    {
        return Cache::get($this->cartKey(), Collection::make([]));
    }

    public function setCart(Collection $cart): Collection
    {
        Cache::put($this->cartKey(), $cart);
        return $cart;
    }

    public function addCart(Product $product, int $quantity = 1): Collection
    {
        $items = $this->getCart()->toArray();
        if (!isset($items[$product->id]))
            $items[$product->id] = 0;
        $items[$product->id] += $quantity;
        if ($items[$product->id] <= 0)
            unset($items[$product->id]);
        return $this->setCart(Collection::make($items));
    }

    public function removeCart(Product $product): Collection
    {
        $items = $this->getCart()->toArray();
        if (isset($items[$product->id]))
            unset($items[$product->id]);
        return $this->setCart(Collection::make($items));
    }

    public function itemsCart(): Collection
    {
        $items = $this->getCart();
        $products = Product::whereIn('id', $items->keys())->get()->keyBy('id');

        return $items->map(function ($quantity, $productId) use ($products) {
            return (object) [
                'product' => $products[$productId] ?? null,
                'quantity' => $quantity,
            ];
        });
    }
    public function itemCount(): string
    {
        $items = $this->getCart()->toArray();
        $quantity = 0;
        foreach ($items as $key => $value) {
            $quantity += $value;
        }

        return $quantity;
    }
}
