<?php

namespace App\Models;

use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $fillable = ['name', 'category_id', 'content', 'price'];
    protected $appends = ['image', 'images'];

    public function getImagesAttribute()
    {
        return $this->getMedia('images');
    }
    public function getImageAttribute()
    {
        return $this->getFirstMediaUrl('images');
    }

    public function scopeOfCategory(Builder $query, ?Category $cat = null): Builder
    {
        return $query->when($cat, function ($query) use ($cat) {
            $query->where('category_id', $cat->id);
        });
    }
}
