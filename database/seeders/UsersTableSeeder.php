<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \DB::table('users')->delete();

        \DB::table('users')->insert(array(
            0 =>
            array(
                'id' => 1,
                'first_name' => 'AlbaSoft',
                'last_name' => 'Bilişim Teknolojileri',
                'phone' => '+90 850 305 2522',
                'email' => 'info@albasoft.com.tr',
                'email_verified_at' => NULL,
                'password' => '$2y$10$Y8pR4gsMNeAGi5iPQSPCnuNSH/nqZhvHdWezJ5TPMXhxnnrTev6rC',
                'remember_token' => NULL,
                'created_at' => '2021-12-16 16:45:59',
                'updated_at' => '2021-12-16 17:11:26',
            ),
            1 =>
            array(
                'id' => 2,
                'first_name' => 'Test',
                'last_name' => 'User',
                'phone' => '+90 111 11 11',
                'email' => 'test@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$Y8pR4gsMNeAGi5iPQSPCnuNSH/nqZhvHdWezJ5TPMXhxnnrTev6rC',
                'remember_token' => NULL,
                'created_at' => '2021-12-16 16:45:59',
                'updated_at' => '2021-12-16 17:11:26',
            ),
        ));
    }
}
