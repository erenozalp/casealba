<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \DB::table('permissions')->delete();

        \DB::table('permissions')->insert(array(
            0 =>
            array(
                'created_at' => '2021-12-16 17:51:59',
                'guard_name' => 'web',
                'id' => 1,
                'name' => 'management.user',
                'updated_at' => '2021-12-16 17:51:59',
            ),
            1 =>
            array(
                'created_at' => '2021-12-16 18:19:07',
                'guard_name' => 'web',
                'id' => 2,
                'name' => 'management.content',
                'updated_at' => '2021-12-16 18:38:09',
            ),
        ));
    }
}
