<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('roles')->delete();

        \DB::table('roles')->insert(array(
            0 =>
            array(
                'id' => 1,
                'name' => 'Admin',
                'guard_name' => 'web',
                'created_at' => '2021-12-16 17:39:53',
                'updated_at' => '2021-12-16 17:44:34',
            ),
            1 =>
            array(
                'id' => 2,
                'name' => 'User',
                'guard_name' => 'web',
                'created_at' => '2021-12-16 17:39:53',
                'updated_at' => '2021-12-16 17:44:34',
            ),
        ));
    }
}
