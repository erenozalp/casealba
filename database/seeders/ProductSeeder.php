<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('products')->delete();

        \DB::table('products')->insert(array(
            0 =>
            array(
                'id' => 1,
                'category_id' => '1',
                'name' => 'X Telefon',
                'content' => 'X telefon hakkında açıklama',
                'price' => '12543',
                'created_at' => '2021-12-16 17:39:53',
                'updated_at' => '2021-12-16 17:44:34',
            ),
            1 =>
            array(
                'id' => 2,
                'category_id' => '2',
                'name' => 'X Kitap',
                'content' => 'X kitap hakkında açıklama',
                'price' => '12',
                'created_at' => '2021-12-16 17:39:53',
                'updated_at' => '2021-12-16 17:44:34',
            ),
        ));
    }
}
