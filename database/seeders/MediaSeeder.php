<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('media')->delete();

        \DB::table('media')->insert(array(
            0 =>
            array(
                'id' => 1,
                'model_type' => 'App\Models\Product',
                'model_id' => '1',
                'uuid' => '437edb4a-ff24-449f-b590-49598f5786d4',
                'collection_name' => 'images',
                'name' => 'phone',
                'file_name' => 'phone.jpg',
                'mime_type' => 'image/jpeg',
                'disk' => 'public',
                'conversions_disk' => 'public',
                'size' => '12173',
                'manipulations' => '[]',
                'custom_properties' => '[]',
                'generated_conversions' => '[]',
                'responsive_images' => '[]',
                'order_column' => '1',
                'created_at' => '2022-12-19 01:13:47',
                'updated_at' => '2022-12-19 01:13:47',
            ),
            1 =>
            array(
                'id' => 2,
                'model_type' => 'App\Models\Product',
                'model_id' => '2',
                'uuid' => '01de1779-4c8b-4bf5-8721-1c7f8d299246',
                'collection_name' => 'images',
                'name' => 'book',
                'file_name' => 'book.jpg',
                'mime_type' => 'image/jpeg',
                'disk' => 'public',
                'conversions_disk' => 'public',
                'size' => '21747',
                'manipulations' => '[]',
                'custom_properties' => '[]',
                'generated_conversions' => '[]',
                'responsive_images' => '[]',
                'order_column' => '1',
                'created_at' => '2022-12-19 01:13:47',
                'updated_at' => '2022-12-19 01:13:47',
            ),
        ));
    }
}
