<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RolesHasPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('role_has_permissions')->delete();

        \DB::table('role_has_permissions')->insert(array(

            array(
                'permission_id' => 1,
                'role_id' => 1,
            ),

            array(
                'permission_id' => 2,
                'role_id' => 1,
            ),
        ));
    }
}
