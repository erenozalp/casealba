<x-front-layout>
    <x-slot name="title">Başlık</x-slot>
    <x-slot name="entryImg">{{ $home->head_pic }}</x-slot>
    <x-slot name="entryText">{{ $home->head_text }}</x-slot>
    <x-slot name="entrySubText">{{ $home->head_sub_text }}</x-slot>
    <x-slot name="entrySubColor">{{ $home->sub_text_color }}</x-slot>
    <x-slot name="entryButtonText">{{ $home->head_button_text }}</x-slot>
    <x-slot name="entryTextColor">{{ $home->head_text_color }}</x-slot>
    <!-- Content section 1-->
    @foreach ($content as $cont)
        <section id="scroll">
            <div class="container px-5">
                <div class="row gx-5 align-items-center">
                    <div class="col-lg-6 {{ $cont->pic_float == 1 ? 'order-lg-2' : '' }}">

                        <div class=" p-5"><img class="img-fluid rounded-circle"
                                src="{{ asset('/storage/' . $cont->resim) }}" alt="..." /></div>
                    </div>
                    <div class="col-lg-6 order-lg-1">
                        <div class="p-5">
                            <h2 class="display-4">{{ $cont->baslik }}</h2>
                            <p>{{ $cont->icerik }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endforeach
    <x-slot name="copyright">{{ $home->copyright_text }}</x-slot>
</x-front-layout>
