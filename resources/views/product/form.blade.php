@php($title = $product->exists ? __('Edit Product') : __('New Product'))
<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ $title }}
        </h2>
    </x-slot>

    <form method="post" enctype="multipart/form-data"
        action="{{ $product->exists ? route('product.update', $product) : route('product.store') }}">
        @csrf
        @if ($product->exists)
            @method('PUT')
        @endif
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <label for="images">{{ __('Images') }}</label>
                        <div class="input-group mb-3">
                            <div class="custom-file">
                                <input id="images" name="images[]" type="file" class="custom-file-input"
                                    id="inputGroupFile01" multiple>
                                <label
                                    class="form-control-custom @error('images') is-invalid @enderror custom-file-label"
                                    for="inputGroupFile01">
                                </label>
                            </div>
                        </div>
                        @error('images')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    @if ($product->exists)
                        <div class="col-md-12">
                            <label>{{ __('Images') }}</label>
                            <div class="form-group">
                                @foreach ($product->getMedia('images') as $image)
                                    <a href="{{ route('mediadelete', $image) }}">
                                        <img src="{{ $image->getUrl() }}" width="70" height="70">
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">{{ __('Name') }}</label>
                            <input id="name" type="text"
                                class="form-control @error('name') is-invalid @enderror" name="name"
                                value="{{ $product->name }}" required autocomplete="name" autofocus>

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="price">{{ __('Price') }}</label>
                            <input id="name" type="number"
                                class="form-control @error('price') is-invalid @enderror" name="price"
                                value="{{ $product->exists ? $product->price : 0 }}" required>

                            @error('price')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="category_id">{{ __('Category') }}</label>
                            <select id="category_id" class="form-control @error('category_id') is-invalid @enderror"
                                name="category_id">
                                <option value="">-</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}"
                                        {{ $product->category_id == $category->id ? 'selected' : '' }}>
                                        {{ $category->name }}
                                    </option>
                                @endforeach
                            </select>

                            @error('category')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="content">{{ __('Content') }}</label>
                            <textarea class="form-control @error('content') is-invalid @enderror" id="content" name="content" rows="3">{{ $product->content }}</textarea>

                            @error('content')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">
                    <i class="fas fa-save"></i>
                    {{ __('Save') }}
                </button>
            </div>
        </div>
    </form>
</x-app-layout>
