<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Products') }}
            @can('management.content')
                <a href="{{ route('product.create') }}" class="btn btn-sm btn-primary float-right">
                    <i class="fas fa-plus"></i>
                    {{ __('Create Product') }}
                </a>
            @endcan
        </h2>
    </x-slot>
    <div class="card">
        <div class="table-responsive">
            <table class="table table-hover">

                <thead class="thead-dark">
                    <tr>
                        <th>{{ __('Image') }}</th>
                        <th>{{ __('Name') }}</th>
                        <th>{{ __('Price') }}</th>
                        <th class="text-right">{{ __('Actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($products as $product)
                        <tr>
                            <td><img src="{{ $product->image }}" alt="" style="height: 75px;"></td>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->price }}</td>

                            <td class="text-right">
                                <a href="{{ route('addToCard', $product) }}" class="btn btn-info"><i
                                        class="fas fa-plus"></i>{{ __('Add To Card') }}</a>
                                @can('management.content')
                                    <a href="{{ route('product.edit', $product) }}" class="btn btn-primary"><i
                                            class="fas fa-pen"></i></a>
                                    <form action="{{ route('product.destroy', $product) }}" method="POST"
                                        class="d-inline-block" onsubmit="return confirm('{{ __('Are you sure?') }}');">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-danger">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>
                                    </form>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
</x-app-layout>
