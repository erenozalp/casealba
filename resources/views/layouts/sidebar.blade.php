<x-side-link href="{{ route('dashboard') }}" icon="fas fa-desktop" :active="request()->routeIs('dashboard')">
    {{ __('Dashboard') }}
</x-side-link>

<x-side-link href="{{ route('category.index') }}" icon="fas fa-layer-group" :active="request()->routeIs('category.*')">
    {{ __('Categories') }}
</x-side-link>
<x-side-link href="{{ route('product.index') }}" icon="fas fa-boxes" :active="request()->routeIs('product.*')">
    {{ __('Products') }}
</x-side-link>

@auth
    @if (auth()->user()->hasAnyPermission(['management.user']))
        <x-side-accordion :title="__('Settings')" icon="fas fa-cogs" :active="request()->routeIs(['user.*'])">

            <x-side-link href="{{ route('user.index') }}" icon="fas fa-users" :active="request()->routeIs('user.*')">
                {{ __('Users') }}
            </x-side-link>
        </x-side-accordion>
    @endif
@endauth
