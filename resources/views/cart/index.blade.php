<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('CART') }}
        </h2>
    </x-slot>
    <div class="card">
        <div class="table-responsive">
            <table class="table table-hover">

                <thead class="thead-dark">
                    <tr>
                        <th>{{ __('Image') }}</th>
                        <th>{{ __('Name') }}</th>
                        <th>{{ __('Price') }}</th>
                        <th>{{ __('Quantity') }}</th>
                        <th></th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($cart as $item)
                        <tr>
                            <td><img src="{{ $item->product->image }}" alt="" style="height: 75px;"></td>
                            <td>{{ $item->product->name }}</td>
                            <td>{{ $item->product->price }}</td>
                            <td>{{ $item->quantity }}</td>
                            <td class="text-right">
                                <a href="{{ route('removeFromCard', $item->product) }}" class="btn btn-danger"><i
                                        class="fas fa-window-close"></i>{{ __('Remove') }}</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
</x-app-layout>
