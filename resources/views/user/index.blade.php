<x-app-layout>
    <x-slot name="header">
        <h2 class="h4   font-weight-bold">
            {{ __('Users') }}
        </h2>
    </x-slot>

    <form>
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">{{ __('Search') }}</span>
                        </div>
                        <input type="text" class="form-control" placeholder="{{ __('Search') }}" name="search"
                            value="{{ $search }}">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-outline-secondary">
                                <i class="fas fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="card">
        <div class="table-responsive">
            <table class="table table-hover">

                <thead class="thead-dark">
                    <tr>
                        <th scope="col">{{ __('First Name') }}</th>
                        <th scope="col">{{ __('Last Name') }}</th>
                        <th scope="col">{{ __('Phone') }}</th>
                        <th scope="col">{{ __('E-mail') }}</th>
                        <th scope="col">{{ __('Role(s)') }}</th>
                        <th scope="col">{{ __('Actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td>{{ $user->first_name }}</td>
                            <td>{{ $user->last_name }}</td>
                            <td>{{ $user->phone }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->getRoleNames() }}</td>
                            <td>
                                <a href="{{ route('user.edit', $user) }}" class="btn btn-primary"><i
                                        class="fas fa-pen"></i></a>

                                <form action="{{ route('user.destroy', $user) }}" method="POST" class="d-inline-block"
                                    onsubmit="return confirm('{{ __('Are you sure?') }}');">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
</x-app-layout>
