<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Home Page') }}
        </h2>
    </x-slot>

    <div class="card my-4">
        <div class="card-body">
            Home Page
        </div>
    </div>
</x-app-layout>
