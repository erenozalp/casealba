<x-app-layout>
    <x-slot name="header">
        <h2 class="h4   font-weight-bold">
            {{ __('Categories') }}
            @can('management.content')
                <a href="{{ route('category.create') }}" class="btn btn-sm btn-primary float-right">
                    <i class="fas fa-plus"></i>
                    {{ __('Create Category') }}
                </a>
            @endcan
        </h2>
    </x-slot>
    <div class="card">
        <div class="table-responsive">
            <table class="table table-hover">

                <thead class="thead-dark">
                    <tr>
                        <th>{{ __('Name') }}</th>
                        <th class="text-right">{{ __('Actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($categories as $category)
                        <tr>
                            <td>{{ $category->name }}</td>

                            <td class="text-right">
                                <a href="{{ route('product.index', ['category' => $category]) }}" class="btn btn-info"><i
                                        class="fas fa-eye"></i>{{ __('Products') }}</a>
                                @can('management.content')
                                    <a href="{{ route('category.edit', $category) }}" class="btn btn-primary"><i
                                            class="fas fa-pen"></i></a>
                                    <form action="{{ route('category.destroy', $category) }}" method="POST"
                                        class="d-inline-block" onsubmit="return confirm('{{ __('Are you sure?') }}');">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-danger">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>
                                    </form>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
</x-app-layout>
