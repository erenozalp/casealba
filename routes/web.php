<?php

use App\Http\Controllers\CardController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('dashboard');
})->name('dashboard');

Route::resource('user', UserController::class)->middleware(['permission:management.user']);

Route::resource('category', CategoryController::class);
Route::resource('product', ProductController::class);


//CART
Route::get('/card/index', [CardController::class, 'index'])->name('cart');
Route::get('/addtocard/{product}', [CardController::class, 'addToCard'])->middleware(['auth'])->name('addToCard');
Route::get('/removefromcard/{product}', [CardController::class, 'removeFromCard'])->middleware(['auth'])->name('removeFromCard');


Route::get('/mediadelete/{media?}', function (Media $media) {
    $media->delete();
    return redirect()->back();
})->middleware(['permission:management.user'])->name('mediadelete');

require __DIR__ . '/auth.php';
