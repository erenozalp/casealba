# Projeyi çalıştırılmadan önce;

    vendor dosyaları için composer install edilmeli.

    .env dosyası düzenmeli ve ASSET_URL set edilmeli.

    application key generate edilmeli.

    image'lerin yüklenebilmesi için storage linklenmeli. 
    (Challange'ta örnek veri oluşması için storage içerisindeki gitignore dosyaları kaldırıldı. Örnek resim eklendi.)

    veritabanı oluşturduktan sonra, migrate seed yapabiliriz.

    veritabanını migrate edip seedledikten sonra dilerseniz .env dosyası içerisindeki CACHE_DRIVER alanını database olarak güncelleyebilirsiniz.
    memcached veya redis kullanımına örnek olması amacıyla cache driver database olarak kullanılmıştır.

    Veritabanında kullanıcı giriş bilgileri hash'li olduğundan dolayı örnek kullanıcı bilgileri aşağıda belirtilmiştir.

Projede oluşturulan örnek kullanıcılar,
    
    mail:info@albasoft.com.tr
    şifre:123123123

    mail:test@mail.com
    şifre:123123123



     


